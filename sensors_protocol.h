#ifndef SENSORS_H
#define SENSORS_H

#ifdef __cplusplus
#else
#include <stdint.h>
#endif

#include "sensor_types.h"

#ifdef __cplusplus
extern "C" {
#endif

void init_pocket(const char* a_device_name, uint8_t a_device_length);

SensorMsg_t parseRadioPayload(const char* a_message, uint8_t messageLenght);

#ifdef __cplusplus
    };
#endif // __cplusplus
#endif // !SENSORS_H

