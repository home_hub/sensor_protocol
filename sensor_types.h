#ifndef SENSOR_TYPES_H
#define SENSOR_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum 
{
    TEMPERATURE_E = 1,
    HUMIDITY_E,
    PRESSURE_E,
    maxSensorTypes,
    specialEnumFor4BitSize = 100000
}SensorType_t;

typedef struct {
    uint8_t cmd : 1; // 0 = sending readings; 1 = sending command. 
    uint8_t unused1 : 1;
    uint8_t unused2 : 1;
    uint8_t unused3 : 1;
    uint8_t unused4 : 1;
    uint8_t unused5 : 1;
    uint8_t unused6 : 1;
    uint8_t unused7 : 1;
}MsgType_t;

typedef struct {
    uint8_t decimal;
    int8_t integral; 
} TempData_t;

typedef struct {
    uint8_t decimal;
    int8_t integral; 
} HumData_t;

typedef struct {
    uint8_t device_id; 
    char device_name[11];
    MsgType_t msg_type;
} MsgHeader_t; 

typedef struct {
SensorType_t sensorType;
    union {
        TempData_t tempData;
        HumData_t humData;
        uint8_t data[9];
    }sensor_data_u;    
}SensorPayload_t;

typedef struct {
    MsgHeader_t header;
    SensorPayload_t sensor_payload; 
}SensorMsg_t;

#ifdef __cplusplus
    };
#endif // __cplusplus
#endif // !SENSOR_TYPES
