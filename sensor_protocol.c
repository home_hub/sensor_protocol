#include "sensors_protocol.h"
#include "sensor_types.h"

#ifdef __cplusplus
#include <cstring>
#include <stdexcept>
#else
#include <stdint.h>
#include <string.h>
#endif

void init_pocket(const char* a_device_name, uint8_t a_device_name_length) 
{}

SensorMsg_t parseRadioPayload(const char* a_message, uint8_t messageLenght)
{
    SensorMsg_t sensorMessage = { 0 };
    if (messageLenght > sizeof(SensorMsg_t)) {
        return sensorMessage;
    }
    memcpy(&sensorMessage.header, a_message, sizeof(sensorMessage.header));
    memcpy(&sensorMessage.sensor_payload, &a_message[13], (messageLenght - sizeof(sensorMessage.header)));
    return sensorMessage;
}
